import logging
import queue
from datetime import datetime as dt, timedelta
import pigpio
from queue import Queue, PriorityQueue, Empty
import pickle
import json

from fade_event import FadeEvent, LightChangeEvent
from secrets import EVENTS_BACKUP_FILENAME
import util

LED_PIN = 12
PWM_FREQ = 1000

current_brightness = 0

pio = pigpio.pi()

uid_registry = util.UidRegistry()

# Read brightness conversion table
duty_cycle_v_brightness = {}
with open('perceived_brightness_0-1000.tsv', 'r') as file:
    for line in file:
        vals = line.split()
        duty_cycle_v_brightness[int(vals[0])] = float(vals[1])


def save_events(events: PriorityQueue, filename: str = EVENTS_BACKUP_FILENAME) -> None:
    with open(filename, 'wb') as f:
        pickle.dump(list(events.queue), f)


def load_event_list_from_file(filename: str) -> PriorityQueue:
    """ Read out the events list that was saved to file in the last run"""
    with open(filename, 'rb') as f:
        events_list = pickle.load(f)

    uid_registry.clear()
    events = PriorityQueue()
    for ev in events_list:
        if ev.finished():
            continue
        events.put(ev)
        try:
            uid_registry.register(ev.uid)
        except util.UidRegistry.UidExistsError:
            logging.warning("Loaded events with conflicting uids. Regenerating...")
            ev.uid = uid_registry.create()

    return events


def clear_current_event(events: PriorityQueue):
    """ If there is an event that is currently active (i.e. we're halfway through a fade), stop or delete it """
    try:
        current_event: FadeEvent = events.get(block=False)
    except Empty:
        return
    if not current_event.currently_active():
        events.put(current_event)
    save_events(events)


def turn_on_now(events: PriorityQueue = None, **_kwargs):
    """ Turn the lights on full brightness, immediately """
    clear_current_event(events)
    return set_to_brightness(100)


def turn_off_now(events: PriorityQueue = None, **_kwargs):
    """ Turn the lights completely off, immediately """
    clear_current_event(events)
    return set_to_brightness(0)


def duty_cycle_from_brightness(brightness: float, **_kwargs) -> float:
    """ Convert a given brightness percentage to a duty cycle (0 to 1) """
    return duty_cycle_v_brightness[int(round(brightness * 10))] / 1000


def set_from_str_and_clear(brightness_str, events: PriorityQueue = None, **_kwargs):
    clear_current_event(events=events)
    return set_to_brightness(float(brightness_str))


def set_to_brightness(brightness: float):
    """ Set a given percentage brightness """
    global current_brightness
    current_brightness = brightness
    duty_cycle = duty_cycle_from_brightness(brightness)
    return pio.hardware_PWM(LED_PIN, PWM_FREQ, int(duty_cycle * 1000000))


def fade_now(brightness_str, fade_period_str, events: PriorityQueue = None, messages_out: Queue = None):
    start_brightness = current_brightness
    end_brightness = int(brightness_str)
    fade_period = util.parse_time_delta_string(fade_period_str)
    return create_fade_event(
        start_brightness,
        end_brightness,
        fade_period,
        dt.now(),
        events=events,
        messages_out=messages_out
    )


def create_fade_event(start_brightness: int,
                      end_brightness: int,
                      fade_period: timedelta,
                      start_time: dt,
                      events: PriorityQueue = None,
                      messages_out: Queue = None,
                      ):
    new_uid = uid_registry.create()
    try:
        new_event = FadeEvent(new_uid, int(start_brightness), int(end_brightness), start_time,
                              fade_period.total_seconds())
    except Exception as e:
        uid_registry.delete(new_uid)
        raise e
    all_events = []
    while not events.empty():
        all_events.append(events.get())
    filtered_events = [new_event]
    filtered_events += [ev for ev in all_events if not new_event.overlaps(ev)]

    filtered_events = [new_event]
    removed_events = []
    for ev in all_events:
        if new_event.overlaps(ev):
            removed_events.append(ev)
        else:
            filtered_events.append(ev)

    if removed_events:
        messages_out.put("Removed conflicting events:\n{}".format('\n'.join(str(ev)) for ev in removed_events))

    for ev in filtered_events:
        events.put(ev)

    save_events(events)


def create_fade_event_from_str(start_brightness_str: str,
                               end_brightness_str: str,
                               fade_period_str: str,
                               start_time_str: str,
                               events: PriorityQueue = None,
                               **_kwargs,
                               ):
    """ Create a fade event and add it to the events queue """
    start_brightness = int(start_brightness_str)
    end_brightness = int(end_brightness_str)
    start_time = dt.strptime(start_time_str, "%Y-%m-%dT%H:%M:%S")
    fade_period_s: timedelta = util.parse_time_delta_string(fade_period_str)
    return create_fade_event(start_brightness, end_brightness, fade_period_s, start_time, events=events)


def _get_filtered_events(
        start_time_str: str = None,
        end_time_str: str = None,
        events: PriorityQueue = None,
        ) -> list:
    """  """
    start_time = None
    end_time = None
    if start_time_str is not None:
        start_time = dt.strptime(start_time_str, "%Y-%m-%dT%H:%M:%S")
    if end_time_str is not None:
        end_time = dt.strptime(end_time_str, "%Y-%m-%dT%H:%M:%S")

    if start_time is not None and end_time is not None:
        filtered_events = [ev for ev in sorted(list(events.queue)) if ev.within_period(start_time, end_time)]
    else:
        filtered_events = sorted(list(events.queue))
    return filtered_events


def str_list_events(
        start_time_str: str = None,
        end_time_str: str = None,
        events: PriorityQueue = None,
        messages_out: Queue = None,
        ):
    filtered_events = _get_filtered_events(start_time_str, end_time_str, events)

    if filtered_events:
        output_str = "Events:\n" + '\n'.join(str(ev) for ev in filtered_events)
    else:
        output_str = "No events scheduled"

    messages_out.put(output_str)


def json_list_events(
        start_time_str: str = None,
        end_time_str: str = None,
        events: PriorityQueue = None,
        messages_out: Queue = None,
        ):
    filtered_events = _get_filtered_events(start_time_str, end_time_str, events)

    events_dict = {
        'events': [event.to_dict(indent=2) for event in filtered_events],
    }

    messages_out.put(json.dumps(events_dict))


def delete_event(event_uid: str, events: PriorityQueue = None, messages_out: Queue = None) -> None:
    """ Delete the specified event from the queue """
    event_uid = int(event_uid)
    all_events = []
    while not events.empty():
        all_events.append(events.get())

    for ev in all_events:
        if ev.uid != event_uid:
            events.put(ev)
        else:
            messages_out.put("Removed event {}: {}".format(ev.uid, str(ev)))


def _current_brightness_description() -> str:
    if current_brightness:
        return f"on at {int(round(current_brightness))}%"
    else:
        return f"off"


def _next_action_description(events: PriorityQueue) -> str:
    try:
        next_event: LightChangeEvent = events.get(block=False)
    except queue.Empty:
        return "no events scheduled"

    if not next_event.finished():
        events.put(next_event)

    if next_event.currently_active():
        return f"currently executing event {next_event.uid}"

    return f"next event {next_event.uid} at {next_event.start_time}"


def send_status(events: PriorityQueue = None, messages_out: Queue = None) -> None:
    messages_out.put(f"{_current_brightness_description().capitalize()}, {_next_action_description(events)}")
