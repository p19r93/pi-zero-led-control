import re
from datetime import timedelta


def parse_time_delta_string(input_string: str) -> timedelta:
    """ Convert a string e.g. '1h', '30m', '2400s' representing a period into a timedelta """
    re_ptn = r'((?P<hours>\d+?)h)?((?P<minutes>\d+?)m)?((?P<seconds>\d+?)s)?'
    match = re.match(re_ptn, input_string)

    hours = int(match.group('hours')) if match.group('hours') is not None else 0
    minutes = int(match.group('minutes')) if match.group('minutes') is not None else 0
    seconds = int(match.group('seconds')) if match.group('seconds') is not None else 0
    return timedelta(hours=hours, minutes=minutes, seconds=seconds)


class UidRegistry:

    class UidExistsError(Exception):
        pass

    class UidDoesNotExistError(KeyError):
        pass

    def __init__(self, current_uids: set = None, max_value: int = 9999):
        self.max_value = max_value
        self.uids_in_use = current_uids if current_uids is not None else set()

    def create(self):
        """
        Registers the next UID and returns it, given those already in use.

        Will return one greater than the current max, unless that max is at or above the recycle limit, in which case it
        will be the lowest unused value greater than 0. If all values are used up to the recycle limit, then keep going
        """
        new_uid = None
        if not self.uids_in_use:
            new_uid = 1
        else:
            curr_max: int = max(self.uids_in_use)
            if curr_max < self.max_value - 1:
                new_uid = curr_max + 1
            else:
                i = 1
                while True:
                    if i not in self.uids_in_use:
                        new_uid = i
                        break
                    i += 1
        assert new_uid is not None
        self.register(new_uid)
        return new_uid

    def delete(self, uid):
        if uid not in self.uids_in_use:
            raise UidRegistry.UidDoesNotExistError("Cannot delete uid {}: uid not registered".format(uid))
        self.uids_in_use.remove(uid)

    def register(self, uid):
        if uid in self.uids_in_use:
            raise UidRegistry.UidExistsError("Uid {} already registered".format(uid))
        else:
            self.uids_in_use.add(uid)

    def clear(self):
        self.uids_in_use = set()
