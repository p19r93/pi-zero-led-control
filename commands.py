from control import turn_on_now, turn_off_now, set_from_str_and_clear, fade_now, create_fade_event_from_str, \
    str_list_events, json_list_events, delete_event, send_status

commands = {
    'on': turn_on_now,
    'off': turn_off_now,
    'set': set_from_str_and_clear,
    'fade': fade_now,
    'fade-at': create_fade_event_from_str,
    'list-events': str_list_events,
    'json-list-events': json_list_events,
    'delete': delete_event,
    'status': send_status,
}
