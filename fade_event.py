from datetime import datetime, timedelta
from abc import ABC, abstractmethod
from typing import List
from croniter import croniter
import re
import json

import util


class LightChangeEvent(ABC):
    duration: timedelta
    _event_type = "abstract"

    def __init__(self, uid: int, brightness_start: float, brightness_stop: float, start_time: datetime):
        self.uid = uid
        self.brightness_start = brightness_start
        self.brightness_stop = brightness_stop
        self.start_time = start_time

    @property
    @abstractmethod
    def end_time(self) -> datetime:
        pass

    @abstractmethod
    def brightness_now(self, time: datetime = None) -> float:
        pass

    def started(self, time: datetime = None) -> bool:
        """ Whether the start time has passed (by the given time, if given, otherwise by the current time) """
        if time is None:
            time = datetime.now()
        return time > self.start_time

    def finished(self, time: datetime = None) -> bool:
        """ Whether the finish time has passed (by the given time, if given, otherwise by the current time) """
        if time is None:
            time = datetime.now()
        return time > self.end_time

    def time_since_start(self, time: datetime = None) -> timedelta:
        """
        The time difference between the given time (if given, otherwise the current time) and the event start time
        """
        if time is None:
            time = datetime.now()

        return time - self.start_time

    def currently_active(self, time: datetime = None) -> bool:
        """ Whether the given time (if given, otherwise the current time) is between the start and finish times """
        if time is None:
            time = datetime.now()
        return self.started(time) and not self.finished(time)

    @property
    def brightness_change(self) -> float:
        """ The net change in brightness from start to end of the event """
        return self.brightness_stop - self.brightness_start

    def overlaps(self, other) -> bool:
        """ Whether this event overlaps another event at all """
        return (self.start_time < other.start_time < self.end_time) \
            or (other.start_time < self.start_time < other.end_time)

    def within_period(self, start_time: datetime, end_time: datetime) -> bool:
        """ Whether this event overlaps the specified period at all """
        assert start_time < end_time
        return (start_time < self.start_time < end_time) \
            or (self.start_time < start_time < self.end_time)

    def generate_next(self, current_time=None):
        """ Returns the next event, if this is part of a recurring series """
        return None

    def to_dict(self):
        return {
            'uid': self.uid,
            'event_type': self._event_type,
            'start_brightness': self.brightness_start,
            'stop_brightness': self.brightness_stop,
            'start_time': self.start_time,
            'end_time': self.end_time,
            'duration': self.duration,
        }

    def to_json(self, indent=None):
        """ Returns a json-format string describing the event """
        return json.dumps(self.to_dict(), indent=indent)

    def __gt__(self, other):
        return self.start_time > other.start_time

    def __lt__(self, other):
        return self.start_time < other.start_time

    def __repr__(self):
        return "<{}: {}% to {}%, starting {}>".format(
            self.__class__.__name__,
            self.brightness_start,
            self.brightness_stop,
            self.start_time.strftime("%Y-%m-%d %H:%M:%S"),
        )


class FadeEvent(LightChangeEvent):
    _event_type = "fade"

    def __init__(self, uid: int, brightness_start: float, brightness_stop: float, start_time: datetime,
                 fade_period_s: float):
        self.duration = timedelta(seconds=fade_period_s)
        self.closed = False  # Flag to declare that the event has been fully dealt with
        super().__init__(uid, brightness_start, brightness_stop, start_time)

    def close(self) -> None:
        self.closed = True

    def brightness_now(self, time: datetime = None) -> float:
        """ What this FadeEvent would set the brightness to, at the given time if given, else now """
        if time is None:
            time = datetime.now()

        if time < self.start_time:
            return self.brightness_start

        elif time > self.end_time:
            return self.brightness_stop

        else:
            elapsed_portion = self.time_since_start(time) / self.duration
            brightness_change_since_start = elapsed_portion * self.brightness_change
            current_brightness = self.brightness_start + brightness_change_since_start
            return current_brightness

    @property
    def end_time(self) -> datetime:
        return self.start_time + self.duration

    def __str__(self):
        return "{:5}: Fade {} at {:g}% at {:s}, ramping to {:g}% over {:s}".format(
            self.uid,
            "started" if self.started() else "starting",
            self.brightness_start,
            self.start_time.strftime("%a %-d %b %Y %H:%M:%S"),
            self.brightness_stop,
            readable_format_timedelta(self.duration)
        )


class InstantLightChange(LightChangeEvent):
    duration = 0
    _event_type = "instant"

    def end_time(self) -> datetime:
        return self.start_time

    def brightness_now(self, time: datetime = None) -> float:
        if time is None:
            time = datetime.now()
        if time < self.start_time:
            return self.brightness_start
        else:
            return self.brightness_stop


class LightChangeEventFactory:
    """ Generates LightChangeEvents """
    # re_ptn = r"\s*(?:(?P<start_brightness>\d{1,3}(?:.\d+)?)-)?(?P<end_brightness>\d{1,3}(?:.[0-9]+)?)(?:\s+(?P<duration>[\dhmsHMS]+))?(?:\s+(?:start|end)(?:(?P<anchor_time>\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2})|(?P<cron_expr>)?))?"
    # re_ptn2 = r"\s*(?:(?P<start_brightness>\d{1,3}(?:.\d+)?)-)?(?P<end_brightness>\d{1,3}(?:.[0-9]+)?)(?:\s+(?P<duration>[\dhmsHMS]+))?(?:\s+(?P<start_or_end>start|end)(?:(?P<anchor_time>\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2})|(?:cron(?P<cron_expr>(?:[\d\,\-\*/]+\s+){3}(?:[\d\,\-\*/\?LWC]+\s+)(?:[\dA-Za-z\,\-\*/]+\s+)(?:[1-7A-Za-z\,\-\*/\?\#]+)(?:\s+[\d\,\-\*/]+)?))))?"
    # re_ptn3 = r"^(?:(?P<start_brightness>\d{1,3}(?:.\d+)?)-)?(?P<end_brightness>\d{1,3}(?:.[0-9]+)?)(?:\s+(?P<duration>[\dhmsHMS]+))?(?:\s+(?P<start_or_end>start|end)\s+(?:(?P<anchor_time>\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2})|(?:cron(?P<cron_expr>(?:[\d\,\-\*/]+\s+){3}(?:[\d\,\-\*/\?LWC]+\s+)(?:[\dA-Za-z\,\-\*/]+\s+)(?:[1-7A-Za-z\,\-\*/\?\#]+)(?:\s+[\d\,\-\*/]+)?))))?$"
    # re_ptn4 = r"^(?:(?P<start_brightness>\d{1,3}(?:.\d+)?)-)?(?P<end_brightness>\d{1,3}(?:.[0-9]+)?)(?:\s+(?P<duration>[\dhmsHMS]+))?(?:\s+(?P<start_or_end>start|end)\s+(?:(?P<anchor_time>\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2})|(?:cron\s+(?P<cron_expr>(?:[\d\,\-\*/]+\s+){3}(?:[\d\,\-\*/\?LWC]+\s+)(?:[\dA-Za-z\,\-\*/]+\s+)(?:[1-7A-Za-z\,\-\*/\?\#]+)(?:\s+[\d\,\-\*/]+)?))))?$"

    re_ptn = r"^(?:(?P<start_brightness>\d{1,3}(?:.\d+)?)-)?(?P<end_brightness>\d{1,3}(?:.[0-9]+)?)(?:\s+(?P<duration>[\dhmsHMS]+))?(?:\s+(?P<start_or_end>start|end)\s+(?:(?P<anchor_time>\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2})|(?:cron\s+(?P<cron_expr>(?:[\d\,\-\*/]+\s+){3}(?:[\d\,\-\*/\?LWC]+\s+)(?:[\dA-Za-z\,\-\*/]+\s+)(?:[1-7A-Za-z\,\-\*/\?\#]+)(?:\s+[\d\,\-\*/]+)?))))?$"
    event_init_matcher = re.compile(re_ptn)

    zero_time = timedelta(seconds=0)

    class InvalidEventCommandString(ValueError):
        pass

    def __init__(self, uid_registry: util.UidRegistry):
        self.uid_registry = uid_registry

    def from_values(self,
                    start_brightness: float,
                    end_brightness: float,
                    start_time: datetime,
                    duration: timedelta = None,
                    ):
        uid = self.uid_registry.create()
        if duration is None or duration == LightChangeEventFactory.zero_time:
            return InstantLightChange(
                uid,
                brightness_start=start_brightness,
                brightness_stop=end_brightness,
                start_time=start_time
            )
        else:
            return FadeEvent(
                uid,
                brightness_start=start_brightness,
                brightness_stop=end_brightness,
                start_time=start_time,
                fade_period_s=duration.total_seconds(),
            )

    def from_anchor_str(self, event_init_str) -> LightChangeEvent:
        """ Create a LightChangeEvent of the appropriate type from a command string """
        # TODO - Create and return the appropriate subclass of LightChangeEvent based on an init string with an anchor time or cron expr
        match = LightChangeEventFactory.event_init_matcher.match(event_init_str)
        start_brightness = float(match.group('start_brightness'))
        end_brightness = float(match.group('end_brightness'))
        duration: timedelta = util.parse_time_delta_string(match.group('duration'))
        start_or_end = match.group('start_or_end')
        anchor_time: datetime = datetime.strptime(match.group('anchor_time'), "%Y-%m-%dT%H:%M:%S")
        cron_expr: str = match.group('cron_expr')

        if start_or_end is None:
            raise LightChangeEventFactory.InvalidEventCommandString("Anchor events must have start or end time specified")

        if anchor_time:
            start_time = anchor_time if start_or_end == 'start' else anchor_time - duration
        elif cron_expr:
            cron = croniter(cron_expr, datetime.now())
            next_cron_time = cron.get_next(datetime.now())
            start_time = next_cron_time if start_or_end == 'start' else next_cron_time - duration
        else:
            raise LightChangeEventFactory.InvalidEventCommandString("I don't know how you got here, but time must be 'start' or 'end'")

        # TODO - you need to somehow give the croniter object or something similar to something that can generate the
        #  next from this. Perhaps you need a new class - a LightEventSeries class which keeps the cron?
        return self.from_values(start_brightness, end_brightness, start_time, duration)

    def from_str_and_start_time(self, event_init_str: str, start_time: datetime) -> LightChangeEvent:
        """
        Create a LightChangeEvent of the appropriate type from a command string, given the start time and start
        brightness separately
        """
        # TODO - this should create and return the appropriate subclass of LightChangeEvent based on the init string
        pass

    def from_str_and_end_time(self, event_init_str: str, end_time: datetime) -> LightChangeEvent:
        """
        Create a LightChangeEvent of the appropriate type from a command string, given the start time and start
        brightness separately
        """
        # TODO - this should create and return the appropriate subclass of LightChangeEvent based on the init string
        pass


def split_seq_command_into_event_strs(cmd_str) -> list:
    return cmd_str.split(';')


class LightChangeEventSequence(LightChangeEvent):
    """ Manages a series of sequential LightChangeEvents as one event """

    class InvalidSequence(ValueError):
        pass

    def __init__(self, uid, events):
        self.events = events
        self.closed: bool = False
        self.duration = sum([ev.duration for ev in self.events])

        super().__init__(
            uid,
            self.events[0].brightness_start,
            self.events[-1].brightness_stop,
            self.events[0].start_time
        )

    @classmethod
    def from_command_str(cls, uid_registry: util.UidRegistry, uid: int, init_command_str: str):
        # TODO - This whole method should move to LightChangeEventFactory, which should take responsibility for all
        #  LightChangeEvent creation, including LightChangeEventSequences. This should also solve the uid_registry
        #  ugliness - the calling module can instantiate a Factory with the registry of its choice
        """
        init_command_str should be a comma-separated list of event string specifiers. There should be exactly one start
        or end anchor-time or cron included. See interface.md
        init_command_str has already had `program` stripped from the beginning
        """
        event_cmd_strs = split_seq_command_into_event_strs(init_command_str)
        events = [None] * len(event_cmd_strs)
        anchor_index = None
        factory = LightChangeEventFactory(uid_registry)
        for i, init_str in enumerate(event_cmd_strs):
            if any(x in init_str for x in ('start', 'end')):
                if anchor_index is not None:
                    raise LightChangeEventSequence.InvalidSequence("You must specify only one start or end time")
                anchor_index = i
                anchor_event = factory.from_anchor_str(init_str)
                events[i] = anchor_event
        if anchor_index is None:
            raise LightChangeEventSequence.InvalidSequence("You must specify a start or end time")

        # Go back from the anchor event filling in the others
        for i in range(anchor_index - 1, -1, -1):
            events[i] = factory.from_str_and_end_time(
                event_cmd_strs[i],
                end_time=events[i+1].start_time
            )

        # Go forward from the anchor event filling in the others
        for i in range(anchor_index + 1, len(events) + 1, 1):
            events[i] = factory.from_str_and_start_time(
                event_cmd_strs[i],
                start_time=events[i-1].end_time
            )

        # for i, init_str in enumerate(self.event_cmd_strs):
        #     if i == 0:
        #         pass
        #     previous_event = self.events[i-1]
        #     this_event = LightChangeEventFactory.from_str_and_start_conds(
        #             event_init_str=init_str,
        #             brightness_start=previous_event.brightness_stop,
        #             start_time=previous_event.end_time,
        #         )
        #     if this_event.duration == 0 and previous_event.duration == 0:
        #         raise LightChangeEventSequence.InvalidSequence("Cannot have two consecutive instant events")
        #     self.events.append(this_event)

        return cls(uid, events)

    def end_time(self) -> datetime:
        return self.events[-1].end_time

    def brightness_now(self, time: datetime = None) -> float:
        if time is None:
            time = datetime.now()
        for event in self.events:
            if event.currently_active(time):
                return event.brightness_now(time)


def readable_format_timedelta(time: timedelta) -> str:
    """ Convert a timedelta into a readable format e.g. 1h10m30s, or 20m, or 1m30s """
    seconds = int(time.total_seconds() % 60)
    minutes = int((time.total_seconds() // 60) % 60)
    hours = int(time.total_seconds() // 3600)
    if hours == 0 and minutes == 0:
        return "{:d}s".format(seconds)
    elif hours == 0:
        return "{:d}m{:d}s".format(minutes, seconds)
    else:
        return "{:d}h{:d}m{:d}s".format(hours, minutes, seconds)
