import threading
from datetime import datetime as dt
import time
from queue import Queue, PriorityQueue
from paho.mqtt import client as mqtt
import logging

from secrets import MQTT_BROKER_ADDR, MQTT_COMMAND_SUB, MQTT_REPLY_SUB, EVENTS_BACKUP_FILENAME
from commands import commands
from fade_event import FadeEvent, LightChangeEvent
import control

# Set the initial brightness
control.set_to_brightness(control.current_brightness)

logging.basicConfig(filename="log.log", level=logging.DEBUG)


def on_connect(client, _userdata, _flags, _rc):
    client.subscribe(MQTT_COMMAND_SUB)
    client.publish(MQTT_REPLY_SUB, "Reconnected")


def setup_mqtt(events: PriorityQueue, messages_out: Queue) -> mqtt.Client:
    client = mqtt.Client("LED controller")
    mqtt_processor = with_events_and_messages_out(events, messages_out)(process_mqtt)
    client.on_message = mqtt_processor
    client.on_connect = on_connect
    client.connect(MQTT_BROKER_ADDR)
    client.loop_start()
    client.publish(MQTT_REPLY_SUB, "Started listening...")

    return client


def run_mqtt(client: mqtt.Client):
    client.loop()


def with_events_and_messages_out(events, messages_out):
    def decorator(func):
        def wrapper(*args, **kwargs):
            kwargs['events'] = events
            kwargs['messages_out'] = messages_out
            return func(*args, **kwargs)
        return wrapper
    return decorator


def process_mqtt(_client, _userdata, message, events=None, messages_out=None):
    """ Process a new message """
    if message.topic != MQTT_COMMAND_SUB:
        return
    msg_payload = str(message.payload.decode('utf-8'))

    logging.debug("Processing message `{}'".format(msg_payload))
    cmd_name, *cmd_args = msg_payload.split(' ')

    # Idea is that a dictionary of handler functions will execute here to run valid commands
    try:
        logging.debug("Calling command name `{}' with arguments {}".format(cmd_name, cmd_args))
        commands[cmd_name](*cmd_args, events=events, messages_out=messages_out)
    except KeyError:
        logging.warning("Received command `{}' not understood".format(msg_payload))
        messages_out.put("Received command `{}' not understood".format(msg_payload))


def send_mqtt_messages(client: mqtt.Client, messages_out: Queue):
    """ Monitor the messages_out queue for things to send """
    while True:
        send_mqtt_message(client, messages_out.get())


def send_mqtt_message(client: mqtt.Client, message: str):
    client.publish(MQTT_REPLY_SUB, message)


def execute_light_commands(events: PriorityQueue):
    """ Control the LEDs based on the events in the doodad """

    while True:
        # Get the next (next to start) event from the queue and put it back immediately if it's not done
        current_event: LightChangeEvent = events.get(block=True)
        current_time = dt.now()
        if not current_event.finished(current_time):
            events.put(current_event)

        # Set the required brightness and check how long before the next change
        if current_event.currently_active(current_time):
            control.set_to_brightness(current_event.brightness_now(current_time))

        # If the time has run out but its final state has not yet been set
        if current_event.finished(current_time) and not current_event.closed:
            control.set_to_brightness(current_event.brightness_stop)
            current_event.close()

        time.sleep(0.01)


def main():
    logging.info("Starting...")

    try:
        events: PriorityQueue[FadeEvent] = control.load_event_list_from_file(EVENTS_BACKUP_FILENAME)
    except FileNotFoundError:
        logging.warning("Previous events file {} not found. Creating new blank events list...".format(
            EVENTS_BACKUP_FILENAME))
        events = PriorityQueue()
    control.save_events(events, EVENTS_BACKUP_FILENAME)
    messages_out = Queue()
    mqtt_client = setup_mqtt(events, messages_out)

    # Prepare the threads
    process_events_thread = threading.Thread(target=execute_light_commands, args=(events,))
    mqtt_listen_thread = threading.Thread(target=run_mqtt, args=(mqtt_client,))
    mqtt_send_messages_thread = threading.Thread(target=send_mqtt_messages, args=(mqtt_client, messages_out,))

    # Start the threads
    mqtt_listen_thread.start()
    process_events_thread.start()
    mqtt_send_messages_thread.start()


if __name__ == "__main__":
    main()
