# Commands

Commands shall be contained within one line
Key commands:
- `on`
- `off`
- `set <brightness>`
- `set-default <brightness>`
- `fade <end_brightness> <duration>`
- `program <comma-separated program string>`
  - Program substrings take the form: `[<start_brightness>-]<end_brightness> [<duration>] [start <start_time>|cron]|[end <end_time>|cron]`
    - Default duration is 0
    - Default start brightness is whatever the brightness was beforehand
  - There must be exactly one start time or end time specified in the whole string, or cron used
  - Examples:
    - `program 0-100 15m end cron 0 30 7 * * MON-FRI, 100 1h, 0`
      This would create an alarm every weekday, fading from 0 to reach 100 at 07:30, then holding at 100 for an hour before switching off
    - `program 100 start 2021-08-01T22:00:00`
- `fade-at [start_brightness] [end_brightness] [duration] [start_time]`
  - Will remove this command I think, in favour of `program`
- `list [from <start_time>] [to <end_time>]` - start and end times are optional, if not specified, all are shown. 
  Response will be list of events, with an id, a start time, duration, start brightness and end 
  brightness
- `delete <event_id>` - delete event with given id

Notes:
- Parameters in square brackets are optional
- `brightness` shall be 0 (off) to 100 (full on), as perceived by a person\
- Times e.g. `start_time` shall be a string in the format yyyy-mm-ddTHH:MM:SS\
- `duration` shall be a string with integer hours (optional), minutes (optional) 
and seconds(optional), in that order, with the suffixes `h`, `m` and `s` after 
each field. For example, `1h`, `10m`, `3m30s`, `600s`, `2h30m30s` would all be valid
